### 数据库技术文章分享快照
- **MySQL**
  - [MySQL在grant语句之后要跟着flush privileges吗？](https://mp.weixin.qq.com/s/Bs57Nb12w_YdTiBWd34Rkw)
  - [MySQL为什么还有kill不掉的语句？](https://mp.weixin.qq.com/s/4VhCmyO2EpHJZrECJp_dxA)
  - [查询请求增加时，如何做主从分离？](https://mp.weixin.qq.com/s/C9HQ15t8rsjsAlLJBAj5xg)
  - [MySQL性能优化一：多种优化 方式介绍](https://mp.weixin.qq.com/s/3qfkfeZOcERR8AB4rzqO8w)
  - [MySQL性能优化(二)：优化数据库的设计](https://mp.weixin.qq.com/s/xafmfOOT3sEuDftdrVvZ8A)
  - [MySQL性能优化(三)：索引](https://mp.weixin.qq.com/s/OxBNSepOs1qFtAwguF8dfw)
  - [MySQL性能优化(四)：分表](https://mp.weixin.qq.com/s/LOY4Wo1ueGAij4CgMuxebw)
  - [MySQL性能优化(五)：分区](https://mp.weixin.qq.com/s/2TPMowGzhhOxZgs_TriMpg)
  - [MySQL性能优化(六)：其他优化](https://mp.weixin.qq.com/s/_NcY_7k02lIm-n5sOn9p2w)
  - [MySQL索引的原理，B+树、聚集索引和二级索引的结构分析](https://mp.weixin.qq.com/s/rWpooijYm3HeQZw3ESZq6w)
  - [如何避免服务停机带来的业务损失?](https://mp.weixin.qq.com/s/L9dL53eCiRcBKeRim4NIBg)
- **Redis**
  - [宕机后，Redis如何实现快速恢复？](https://mp.weixin.qq.com/s/5GHiMu_KxmHW1sVsPsuXvw)
  - [Redis主从复制以及主从复制原理](https://mp.weixin.qq.com/s/0Y72MGcah8xcIJHU5f5NxA)
  - [用Redis构建缓存集群的最佳实践有哪些？](https://mp.weixin.qq.com/s/5eSAl8XpGgD-ZFs4ycdeXg)
  - [哨兵机制：主库挂了，如何不间断服务？](https://mp.weixin.qq.com/s/3eqOZItXMsj0rt3GNF6HOA)
  
  
  <div align="center">
      <img src="https://images.gitee.com/uploads/images/2020/0821/220601_c08feacc_1468963.png" width="500px">
  </div>